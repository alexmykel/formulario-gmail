﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="prueba.aspx.cs" Inherits="formulario.prueba" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
    <head runat="server">
          <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
  
        <title>Formulario en ASP</title>
        <link href="css/bootstrap.min.css" rel="stylesheet" />
    </head>
    <body style="background-image: linear-gradient(grey, #f06d06);">

        <form id="form1" runat="server">
            <div class="container">
                <div class="row">
                    <div class="col-sm-5 col-sm-offset-1">
                        <h2>Formulario</h2>
                                <!-- etiqueta de nombre-->
                             <div class="form-group">
                                 <label>Nombre</label>
                                 <asp:TextBox ID="txtName" CssClass="form-control" onkeypress="return letras(event)" title="Introduzca un nombre coherente que empiece por mayúscula" pattern="[A-Z]{1}[a-z]+|[A-Z]{1}[a-z]+\s[A-Z]{1}[a-z]+" runat="server" required="required" MaxLength="20" BorderWidth="2px"></asp:TextBox>
                             </div> 
                                <!-- etiqueta de apellido-->
                            <div class="form-group">
                                 <label>Apellidos</label>
                                 <asp:TextBox ID="txtSurname" CssClass="form-control" onkeypress="return letras(event)" title="Introduzca un apellido coherente que empiece por mayúscula" pattern="[A-Z]{1}[a-z]+|[A-Z]{1}[a-z]+\s[A-Z]{1}[a-z]+" runat="server" required="required" MaxLength="25" BorderWidth="2px"></asp:TextBox>
                            </div> 
                                <!-- etiqueta DNI-->
                            <div class="form-group">
                                 <label>DNI</label>
                                 <asp:TextBox ID="txtDNI" CssClass="form-control" title="Introduzca un DNI correcto" pattern="(([X-Z]{1})([-]?)(\d{7})([-]?)([A-Z]{1}))|((\d{8})([-]?)([A-Z]{1}))" runat="server"  required="required" MaxLength="9" BorderWidth="2px"></asp:TextBox>
                            </div> 
                                <!-- etiqueta no visible Email-->
                            <div class="form-group">
                               <asp:TextBox ID="txtEmail" CssClass="form-control" runat="server">prueba@email.com</asp:TextBox>
                            </div> 
                                <!-- etiqueta no visible asunto-->
                            <div class="form-group">
                               <asp:TextBox ID="txtSubject" CssClass="form-control" runat="server">Asunto oculto</asp:TextBox>
                            </div> 


                        <div class="form-group">
                                <!-- etiqueta para comprobar errores y boton para envio de formulario-->
                           <asp:Label CssClass="error"  ID="Labelerror" runat="server"></asp:Label>
                            <asp:Button ID="Button1" CssClass="btn btn-primary" runat="server" OnClick="Button1_Click" Text="Enviar información" BackColor="#00FF99" BorderColor="#00CC00" Font-Bold="True" Font-Italic="True" Font-Overline="False" ForeColor="Maroon" />
                        </div>

                     </div>  
                </div>
            </div>                                                                     
        </form>


        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery.js"></script>
        <script src="js/main.js"></script>
        <script>         
            <!-- Javascript que no permite incluir numeros-->
            function letras(evt){
                var charCode = (evt.which) ? evt.which : event.keyCode
                if (charCode > 31 && (charCode < 48 || charCode > 57))
                    return true;
                    return false;
            }
            </script>
    </body>
</html>

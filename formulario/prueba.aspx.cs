﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace formulario
{
    public partial class prueba : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Ocultamos del codigo fuente los textbox de asunto y email al cargar la pagina
            txtSubject.Visible = false;
            txtEmail.Visible = false;
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
                // Creamos variable donde guardaremos los datos a remitir en el correo
                string message;

                // Mensaje que enviaremos por correo
                message = "<h2 style=\"color:red\";>Formulario </h2> <ul style=\"list - style - type:square; \"><li><b>Nombre:</b> " + txtName.Text +  "</li><li><b>Apellidos:</b> " + txtSurname.Text + "</li><li><b>DNI:</b> " + txtDNI.Text + "</li> </ul>" +
                "<p><b>Puede seguir haciendo uso de este formulario a traves de este <a href=\"http://alejandronecom.somee.com/prueba.aspx \">enlace</b></a></p>";

                // configuración del correo 
                System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();
                mail.From = new System.Net.Mail.MailAddress(txtEmail.Text);
                // Email receptor
                mail.To.Add("AQUI INCLUYES EL EMAIL DE LA PERSONA QUE QUIERES ENVIAR EL MENSAJE");
                string x = txtSubject.Text;
                // Asunto de mensaje
                mail.Subject = "Datos del formulario";
                // El mensaje que enviaremos por correo lo incluimos en el cuerpo
                mail.Body = message;
                // Enviamos con codigo html el correo
                mail.IsBodyHtml = true;
                //Prioridad de mensaje (Normal)
                mail.Priority = System.Net.Mail.MailPriority.Normal;

                // Credenciales para  conectar con el servidor de GMAIL

                // Conectamos con el puerto 587
                System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient("smtp.gmail.com", 587);

                // Incluimos credencial de acceso al email que emitirá el correo automático (usuario + password)
                smtp.Credentials = new System.Net.NetworkCredential("AQUI INCLUYES TU CORREO GMAIL", "AQUI DEBES ESCRIBIR LA CONTRASEÑA DE TU CORREO");
                smtp.EnableSsl = true;

                // Realizamos proceso de excepción de errores
                try
                {
                    // Asignamos envio de correo
                    smtp.Send(mail);
                    // Si se remite el correo
                    Labelerror.Text = "<b>El formulario ha sido enviado satisfactoriamente</b>";
                    Button1.Visible = false;

                }
                catch (Exception ex)
                {
                    //Si se produce catch mostraremos el mensaje de error recogido;
                    Labelerror.Text = "Se ha producido un error: " + ex.Message.ToString();
                }
        }
    }
}